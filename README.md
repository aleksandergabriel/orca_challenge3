# orca_challenge



##How to build code
```
cd path/to/orca_challenge
mkdir build
cd build
cmake ..
make
./shortest_path -h
```

##Visualization
Solution can be visualized using simple python3 script `plot.py`. 
Run it in the same folder where the rest of files were extracted.
It takes path to file containing shortest path as parameter `-p`   
Example: `python3 plot.py -p build/shortest_path.json`