import json
from matplotlib import pyplot as plt
import argparse
import os


class Point:
    def __init__(self, _x, _y, _label):
        self.x = _x
        self.y = _y
        self.label = _label

class Gate:
    def __init__(self, _p1, _p2):
    # def __init__(self, _left, _right):
        if _p1.x != _p2.x:
            self.vertical = False
            if _p1.x < _p2.x:
                self.left = _p1
                self.right = _p2
            else:
                self.left = _p2
                self.right = _p1
        else:
            self.vertical = True
            if _p1.y > _p2.y:
                self.left = _p1
                self.right = _p2
            else:
                self.left = _p2
                self.right = _p1



parser = argparse.ArgumentParser()
parser.add_argument("-p", "--path", type=str, help="path to a file containing shortest path")
parser.add_argument("-po", "--points", type=str, default="points.json", help="Path to a file containing points")
parser.add_argument("-se", "--start_end", type=str, default="start_end.json", help="Path to a file containing start and end points")
args, unknown = parser.parse_known_args()

if not args.path:
    print("ERROR: Missing file with calculated path")
    exit(1)
elif not os.path.isfile(args.path):
    print(f"ERROR: {args.path} not file.")
    exit(1)

if not os.path.isfile(args.points):
    print(f"ERROR: {args.points} not file.")
    exit(1)
if not os.path.isfile(args.start_end):
    print(f"ERROR: {args.start_end} not file.")
    exit(1)

f_points = open(args.points, "r")
f_goals = open(args.start_end, "r")
f_path = open(args.path, "r")

points_arr = []
if f_points:
    points_json = json.load(f_points)
    f_points.close()
    for p in points_json:
        points_arr.append(Point(p["x"], p["y"], p["label"]))

    print(len(points_arr))

start_point = None
target_point = None

if f_goals:
    goals_json = json.load(f_goals)
    f_goals.close()
    for p in goals_json:
        if p["label"] == "FROM":
            start_point = Point(p["x"], p["y"], p["label"])
        elif p["label"] == "TO":
            target_point = Point(p["x"], p["y"], p["label"])

final_path = []
if f_path:
    path_json = json.load(f_path)
    f_path.close()
    for p in path_json:
        final_path.append(Point(p["x"], p["y"], p["label"]))

gates = []
for i in range(int(len(points_arr)/2)):
    gates.append(Gate(points_arr[i*2], points_arr[i*2+1]))


for g in gates:
    xs = [g.left.x, g.right.x]
    ys = [g.left.y, g.right.y]
    plt.plot(xs, ys)

# print(total_path)
xss = []
yss = []
for p in final_path:
    xss.append(p.x)
    yss.append(p.y)
plt.plot(xss, yss)
for p in final_path:
    plt.annotate(p.label, (p.x, p.y))

plt.show()


