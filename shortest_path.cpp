#include <iostream>
#include <cmath>
#include <fstream>
#include "json.hpp"
#include "boost/filesystem.hpp"
#include "boost/program_options.hpp"


class Point {

public:
    double x;
    double y;
    std::string label;

    Point() {};

    Point(double _x, double _y, std::string _label) :
            x{_x}, y{_y}, label{_label} {}

    nlohmann::json getJson() {
        nlohmann::json j;
        j["x"] = x;
        j["y"] = y;
        j["label"] = label;
        return j;
    }
};

double getK(Point p1, Point p2) {
    return (p2.y - p1.y) / (p2.x - p1.x);
};


double getN(Point p, double k) {
    return p.y - k * p.x;
};

double getDistanceSquared(Point &p1, Point &p2) {
    return pow((p1.x - p2.x), 2) + pow((p1.y - p2.y), 2);
};

class Gate {

public:
    Point *left;
    Point *right;

    double k;
    double n;

    bool vertical;

    Gate(Point *_p1, Point *_p2) {
        if (_p1->x != _p2->x) {
            vertical = false;
            if (_p1->x < _p2->x) {
                left = _p1;
                right = _p2;
            } else {
                left = _p2;
                right = _p1;
            }
        } else {
            vertical = true;
            if (_p1->y > _p2->y) {
                left = _p1;
                right = _p2;
            } else {
                left = _p2;
                right = _p1;
            }
        }

        k = getK(*left, *right);
        n = getN(*left, k);

    };

    bool crosses(double _k, double _n) {
        if (_k == k) {
            return false;
        }
        double x = (_n - n) / (k - _k);
        double y = _k * x + _n;
        if (!vertical) {
            if (x > left->x and x <= right->x) {
                return true;
            } else {
                return false;
            }
        } else {
            if (y < left->y and y > right->y) {
                return true;
            } else {
                return false;
            }
        }
    }

};

struct passedGates {
    Point *start_point;
    Point *target_point;
    std::vector<Gate *> gates;

    passedGates(Point *_start_point, Point *_target_point, std::vector<Gate *> _gates) :
            start_point(_start_point), target_point(_target_point), gates(_gates) {};
};


Point *getClosesPoint(Point &target, Gate &gate) {

    double dist_left = getDistanceSquared(*gate.left, target);
    double dist_right = getDistanceSquared(*gate.right, target);
    if (dist_right < dist_left) {
        return gate.right;
    } else {
        return gate.left;
    }

}

bool checkIfLinePassesGate(Point &start, Point &stop, std::vector<Gate *> gates) {

    double k_tmp = getK(start, stop);
    double n_tmp = getN(start, k_tmp);

    for (auto g: gates) {
        if (!g->crosses(k_tmp, n_tmp)) {
            return false;
        }
    }
    return true;
}

void exportPath(std::vector<Point *> path, std::string filename) {
    std::cout << "Exporting path" << std::endl;

    nlohmann::json j_out = nlohmann::json::array();

    for (auto p : path) {
        j_out.push_back(p->getJson());
    }

    std::ofstream o(filename);
    o << std::setw(4) << j_out << std::endl;
    o.close();
}

bool checkPath(std::string &path){
    if (!boost::filesystem::exists(boost::filesystem::absolute(path))) {
        std::cout << "File on path " << boost::filesystem::absolute(path) << " does not exist. Please enter valid path." << std::endl;
        return false;
    }
    return true;
}

int main(int argc, char **argv) {

    std::string points_path, target_path;

    try {
        boost::program_options::options_description desc{"Options"};
        desc.add_options()
                ("help,h", "Help screen")
                ("points_file,pf", boost::program_options::value<std::string>(), "Path to json file containing points for path calculation.")
                ("target_file,tf", boost::program_options::value<std::string>(), "Path to json file containing start and finish points for path calculation.");

        boost::program_options::variables_map vm;
        boost::program_options::store(parse_command_line(argc, argv, desc), vm);
        boost::program_options::notify(vm);

        if (vm.count("help")) {
            std::cout << desc << '\n';
            return 0;
        } else {
            if (vm.count("points_file")) {
                points_path = vm["points_file"].as<std::string>();
                if (!checkPath(points_path)) {
                    return 1;
                }
            }
            if (vm.count("target_file")) {
                target_path = vm["target_file"].as<std::string>();
                if (!checkPath(target_path)) {
                    return 1;
                }
            }
        }

    }
    catch (const boost::program_options::error &ex) {
        std::cerr << ex.what() << '\n';\
    return 1;
    }

    std::ifstream f_points(points_path);
    std::ifstream f_goals(target_path);

    std::string points_json_string, goals_json_string, tmp_string;

    while (std::getline(f_points, tmp_string)) {
        points_json_string += tmp_string;
    }
    f_points.close();

    while (std::getline(f_goals, tmp_string)) {
        goals_json_string += tmp_string;
    }

    nlohmann::json j, targets_j;

    std::cout << "parsing jsons" << std::endl;
    j = nlohmann::json::parse(points_json_string);
    targets_j = nlohmann::json::parse(goals_json_string);
    f_points.close();
    f_goals.close();


    std::vector<Point> points;
    Point start_point, target_point;
    for (auto p : j) {
        points.push_back(Point(p["x"], p["y"], p["label"]));
    }

    for (auto p : targets_j) {
        if (p["label"] == "FROM") {
            start_point = Point(p["x"], p["y"], p["label"]);
        } else {
            // todo check for bad entry
            target_point = Point(p["x"], p["y"], p["label"]);
        }
    }

    std::vector<Gate> gates;
    for (int i = 0; i < points.size() / 2; ++i) {
        gates.push_back(Gate(&points[i * 2], &points[i * 2 + 1]));
    }

    std::cout << "calculating path" << std::endl;

    int gate_iter = 0;
    std::vector<Point *> total_path = {&start_point};
    Point *local_path_start = &start_point;
    Point *local_path_end = getClosesPoint(target_point, gates[gate_iter]);

    std::vector<Gate *> local_path_gates = {&gates[0]};

    std::vector<passedGates> passes_gates;

    while (true) {
        gate_iter += 1;
        if (gate_iter == gates.size()) {
            break;
        }

        Point *tmp_end = getClosesPoint(target_point, gates[gate_iter]);
        bool succ = checkIfLinePassesGate(*local_path_start, *tmp_end, local_path_gates);

        if (succ) {
            local_path_gates.push_back(&gates[gate_iter]);
            local_path_end = tmp_end;
            continue;
        } else {
            passedGates tmp_passed_gates(*total_path.rbegin(), local_path_end, local_path_gates);
            passes_gates.push_back(tmp_passed_gates);

            total_path.push_back(local_path_end);
            local_path_gates = {&gates[gate_iter]};
            local_path_start = local_path_end;
            local_path_end = tmp_end;
        }


    }

    double k_tmp = getK(*local_path_start, target_point);
    double n_tmp = getN(*local_path_start, k_tmp);
    bool succ = true;
    for (auto g: local_path_gates) {
        if (!g->crosses(k_tmp, n_tmp)) {
            succ = false;
            break;
        }
    }
    if (!succ) {
        passedGates tmp_passed_gates(*total_path.rbegin(), local_path_end, local_path_gates);
        passes_gates.push_back(tmp_passed_gates);
        total_path.push_back(local_path_end);
    }
    passedGates tmp_passed_gates(*total_path.rbegin(), &target_point, local_path_gates);
    passes_gates.push_back(tmp_passed_gates);
    total_path.push_back(&target_point);

    std::cout << "Current shortest path:" << std::endl;
    for (auto p : total_path) {
        std::cout << p->label << " -> ";
    }

    std::cout << std::endl;

    std::cout << "Second pass" << std::endl;
    std::vector<Point *> second_path = {&start_point};
    for (auto iter = passes_gates.begin(); iter <= passes_gates.end();) {

        if (iter + 1 == passes_gates.end()) {
            second_path.push_back(iter->target_point);
            break;
        }
        std::cout << "Checking between " << iter->start_point->label << " and " << (iter + 1)->target_point->label
                  << std::endl;

        Point *new_start = iter->start_point;
        Point *new_end = (iter + 1)->target_point;
        if (checkIfLinePassesGate(*new_start, *new_end, iter->gates) &&
            checkIfLinePassesGate(*new_start, *new_end, (iter + 1)->gates)) {
            std::cout << "Passes both!" << std::endl;
            second_path.push_back((iter + 1)->target_point);
            iter += 2;
        } else {
            second_path.push_back(iter->target_point);
            iter += 1;
        }

    }

    if (second_path.back()->label != target_point.label) {
        second_path.push_back(&target_point);
    }

    exportPath(second_path, "shortest_path.json");

    return 0;
}

